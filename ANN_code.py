import skimage.io as io
import skimage.transform as trans
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as keras
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras import utils

import numpy as np 
import os
import shutil
import glob
import skimage.io as io
import skimage.transform as trans
import matplotlib.pyplot as plt
from PIL import Image
from IPython.display import clear_output
import datetime

class UnetModel:
    def __init__(self):
        pass

    def dice_coef(self, y_true, y_pred):
        return (2. * K.sum(y_true * y_pred) + 1.) / (K.sum(y_true) + K.sum(y_pred) + 1.)

    def unet(self, input_size = (224, 224, 3), pretrained_weights = None):
        inputs = Input(input_size)

        conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
        conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
        pool1 = MaxPooling2D(pool_size = (2, 2))(conv1)
        conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
        conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
        pool2 = MaxPooling2D(pool_size = (2, 2))(conv2)
        conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
        conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
        pool3 = MaxPooling2D(pool_size = (2, 2))(conv3)
        conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
        conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
        batch4 = BatchNormalization()(conv4)
        drop4 = Dropout(0.5)(batch4)
        pool4 = MaxPooling2D(pool_size = (2, 2))(drop4)
    
        conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
        conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
        batch5 = BatchNormalization()(conv5)
        drop5 = Dropout(0.5)(batch5)

        up6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2, 2))(drop5))
        merge6 = concatenate([drop4, up6], axis = 3)
        conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
        conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

        up7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2, 2))(conv6))
        merge7 = concatenate([conv3, up7], axis = 3)
        conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
        conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

        up8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2, 2))(conv7))
        merge8 = concatenate([conv2, up8], axis = 3)
        conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
        conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

        up9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2, 2))(conv8))
        merge9 = concatenate([conv1, up9], axis = 3)
        conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
        conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
        conv9 = Conv2D(3, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
        conv10 = Conv2D(3, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(), loss = 'binary_crossentropy', metrics = [self.dice_coef]) 
    
    if(pretrained_weights):
        model.load_weights(pretrained_weights)
    
    return model

class Main:
    def __init__(self, source_x_train_path=None, source_y_train_path=None):
        self.MyNet = UnetModel().unet(input_size = (224, 224, 3))
        self.source_x_train_path = source_x_train_path
        self.source_y_train_path = source_y_train_path

        self.prediction_list = []

    def Train_Generator(self, image_generator=None, labels_generator=None):
        while True:
            x = image_generator.next() #or next(train_generator)
            y = labels_generator.next() #or next(train_generator1)
            yield (x[0],y[0])


    def Valid_Generator(self, val_image_generator=None, val_labels_generator=None):
        while True:
            x = val_image_generator.next() #or next(train_generator)
            y = val_labels_generator.next() #or next(train_generator1)
            yield (x[0],y[0])


    def datagen(self, path_x_train=None, path_y_train_ohe=None, path_x_test=None, path_y_test=None, pack_counter=None):
        bs=5
        seed = 1

        datagen = ImageDataGenerator(rescale=1./255)

        image_generator = datagen.flow_from_directory(
            path_x_train,
            target_size = (224, 224),
            batch_size = 10,
            class_mode = 'binary')

        labels_generator = datagen.flow_from_directory(
            path_y_train_ohe,
            target_size = (224, 224),
            batch_size = 10,
            class_mode = 'binary')

        val_image_generator = datagen.flow_from_directory(
            path_x_test,
            target_size = (224, 224),
            batch_size = 5,
            class_mode = 'binary')

        val_labels_generator = datagen.flow_from_directory(
            path_y_test,
            target_size = (224, 224),
            batch_size = 5,
            class_mode = 'binary')

        train_generator = self.Train_Generator(image_generator, labels_generator)
        validation_generator = self.Valid_Generator(val_image_generator, val_labels_generator)

        import keras
        callbacks_list = [
                          keras.callbacks.EarlyStopping(
                              monitor='val_acc',
                              patience=10,
                              ),
                          keras.callbacks.ModelCheckpoint(
                              filepath='/content/drive/My Drive/U-Net/DataSet4/trees_and_fields_callbacks_ver{0}.h5'.format(pack_counter+1), 
                              monitor='val_loss',
                              save_best_only=True,
                              )
                          ]

        history = self.MyNet.fit_generator(
            train_generator,
            steps_per_epoch = 200,
            epochs=10,
            callbacks=callbacks_list,
            validation_data=validation_generator,
            validation_steps = 10
        )

    def train_unet(self):

        path_x_train = ''
        path_y_train_ohe = ''
        path_x_test = ''
        path_y_test = ''

        for i in range(10):
            self.datagen(path_x_train, path_y_train_ohe, path_x_test, path_y_test, i)

            i_path = '/content/drive/My Drive/U-Net/DataSet4/X_test/1'
            i_file = os.listdir(i_path)
            img = image.load_img(os.path.join(i_path, i_file[0]), target_size=(224,224))

            buf = image.img_to_array(img)
            x_test_image = np.expand_dims(buf, axis=0)

            predictions =M7.MyNet.predict(x_test_image)
            predictions_name = '/content/drive/My Drive/U-Net/DataSet4/predictions_{0}'.format(i)
            np.save(predictions_name, predictions)

            self.prediction_list.append(predictions)
            
class Test:
    def __init__(self, num_pack=0, num_img=0, source_x_test_path=None, source_y_test_path=None):
        self.num_img = num_img
        self.num_pack = num_pack
        self.source_x_test_path = source_x_test_path
        self.source_y_test_path = source_y_test_path

    def getImage(self, img_old): #тензор (height,weight),class
        img_new=np.zeros((224,224,4))
        for i in range(img_old.shape[0]):
            for j in range(img_old.shape[1]):
                lb=img_old[i,j]
                if lb==0:
                    img_new[i,j,0]=255.
                    img_new[i,j,1]=255.
                    img_new[i,j,2]=255.
                    img_new[i,j,3]=0.
                if lb==1:
                    img_new[i,j,0]=0.
                    img_new[i,j,1]=255.
                    img_new[i,j,2]=0.
                    img_new[i,j,3]=255.

        return img_new/255



    def test(self):
        predictions =M7.MyNet.predict(np.expand_dims(x_test[self.num_img], axis=0))
        y_prediction = np.argmax(predictions[0], axis=2)

        img_test_arr=self.getImage(y_test_labels)
        img_pred_arr=self.getImage(y_prediction)

        plt.imshow(img_test_arr)
        plt.show()

        plt.imshow(img_pred_arr)
        plt.show()


        x_path = ''
        x_list = os.listdir(x_path)
        x_list = sorted(x_list)
        img = image.load_img(os.path.join(x_path, x_list[1]), target_size=(224,224))
        plt.imshow(img)
        plt.show() 
            
            
if __name__ == "__main__":
    M7 = Main('/content','/content')
    M7.train_unet()
    
    T = Test(0, 1,'','',)
    T.test()

